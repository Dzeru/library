package com.dzeru.library.service;

import com.dzeru.library.dto.CountBooksByGenreReportDto;
import com.dzeru.library.entity.Genre;
import com.dzeru.library.repository.BookRepository;
import com.dzeru.library.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.GenericStoredProcedure;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private DataSource dataSource;

    public List<CountBooksByGenreReportDto> countBooksByGenre() {
        List<Genre> genres = genreRepository.findAll();
        List<CountBooksByGenreReportDto> genresCount = new ArrayList<>();

        StoredProcedure procedure = new GenericStoredProcedure();
        procedure.setDataSource(dataSource);
        procedure.setSql("CountBooksByGenre");
        procedure.setFunction(false);

        SqlParameter[] parameters = {
                new SqlParameter(Types.VARCHAR),
                new SqlOutParameter("res", Types.INTEGER)
        };

        procedure.setParameters(parameters);
        procedure.compile();

        for(Genre g : genres) {
            Map<String, Object> result = procedure.execute(g.getGenre());
            genresCount.add(new CountBooksByGenreReportDto(g.getGenre(), (Integer) result.get("res")));
        }

        return genresCount;
    }

    public Map<Integer, String> checkBookTitles() throws Exception {
        Map<Integer, String> statusMap = new HashMap<>();
        int status = bookRepository.checkBookTitles();
        if(status == 0) {
            statusMap.put(0, "Ошибок в заголовках нет");
        }
        else {
            statusMap.put(1, "Ошибки в заголовках!");
        }
        return statusMap;
    }

}
