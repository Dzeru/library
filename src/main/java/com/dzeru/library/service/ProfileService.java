package com.dzeru.library.service;

import com.dzeru.library.entity.Librarian;
import com.dzeru.library.entity.ReaderForm;
import com.dzeru.library.repository.LibrarianRepository;
import com.dzeru.library.repository.ReaderFormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class ProfileService {

    @Autowired
    private LibrarianRepository librarianRepository;

    @Autowired
    private ReaderFormRepository readerFormRepository;

    public Map<String, Boolean> updateProfileLibrarian(String id,
                                                       String surname,
                                                       String firstName,
                                                       String patronymic,
                                                       String jobPosition) {
        Map<String, Boolean> status = new HashMap<>();
        int librarian = librarianRepository.updateLibrarian(id, surname, firstName, patronymic, jobPosition);

        if(librarian > 0) {
            status.put("success", true);
        }
        else {
            status.put("error", false);
        }

        return status;
    }

    public Map<String, Boolean> updateProfileReader(String id,
                                                    String surname,
                                                    String firstName,
                                                    String patronymic,
                                                    Date birthdayDate,
                                                    String birthdayPlace,
                                                    String education,
                                                    String job,
                                                    String phone,
                                                    String email,
                                                    Date registrationDate,
                                                    String officialAddress,
                                                    String factAddress) {
        Map<String, Boolean> status = new HashMap<>();
        int readerForm = readerFormRepository.updateReader(
                id,
                surname, firstName, patronymic,
                birthdayDate, birthdayPlace,
                education, job,
                phone, email,
                registrationDate, officialAddress, factAddress
        );

        if(readerForm > 0) {
            status.put("success", true);
        }
        else {
            status.put("error", false);
        }

        return status;
    }
}
