package com.dzeru.library.service;

import com.dzeru.library.dto.DeleteBookDto;
import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.Book;
import com.dzeru.library.entity.BookIssuedByReader;
import com.dzeru.library.entity.ReaderForm;
import com.dzeru.library.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.sql.Date;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private NewBookRepository newBookRepository;

    @Autowired
    private BookIssuedByReaderRepository bookIssuedByReaderRepository;

    @Autowired
    private ReaderFormRepository readerFormRepository;

    @Autowired
    private BookIssuedRepository bookIssuedRepository;

    public Map<String, String> addBook(String title,
                          String pages,
                          String ISBN,
                          String ISSN,
                          String UDC,
                          String issueYear,
                          Date issueDate,
                          String edition,
                          String circulation,
                          String kind,
                          String series,
                          String cover,
                          String editionPlace,
                          String publishingId) {
        Map<String, String> status = new HashMap<>();

        int pagesInt, circulationInt, issueYearInt;
        Long publishingIdLong;
        if(StringUtils.isEmpty(title)) {
            status.put("error", "Заголовок не может быть пустым!");
            return status;
        }

        try {
            pagesInt = Integer.parseInt(pages);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение количества страниц!");
            return status;
        }

        try {
            circulationInt = Integer.parseInt(circulation);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение тиража!");
            return status;
        }

        try {
            issueYearInt = Integer.parseInt(issueYear);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение года издания!");
            return status;
        }

        try {
            publishingIdLong = Long.parseLong(publishingId);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение id издательства!");
            return status;
        }

        int newBook = bookRepository.insertBook(title,
                pagesInt,
                ISBN,
                ISSN,
                UDC,
                issueYearInt,
                issueDate,
                edition,
                circulationInt,
                kind,
                series,
                cover,
                editionPlace,
                publishingIdLong);

        if(newBook > 0) {
            status.put("success", "Книга сохранена!");
        }
        else {
            status.put("error", "Не удалось добавить книгу!");
        }

        return status;
    }

    public List<DeleteBookDto> findBookByTitle(String title) {
        List<Book> books = bookRepository.findBookByTitle(title);
        return books.stream().map(DeleteBookDto::new).collect(Collectors.toList());
    }

    public Map<String, String> deleteBooks(String bookIdsForDelete) {
        Map<String, String> status = new HashMap<>();
        List<String> ids = Arrays.asList(bookIdsForDelete.split(","));
        List<Long> idsLong = ids.stream().map(Long::parseLong).collect(Collectors.toList());
        int deleted = bookRepository.deleteBooksById(idsLong);

        if(deleted > 0) {
            status.put("success", "Удалено.");
        }
        else {
            status.put("error", "Не удалось удалить!");
        }
        return status;
    }

    public Map<String, Object> findBookByTitleOrId(String param) {
        Map<String, Object> result = new HashMap<>();
        Book book;
        boolean withId = true;

        Long id = -1L;

        try {
            id = Long.parseLong(param);
        }
        catch(Exception e) {
            withId = false;
        }

        if(withId) {
            book = bookRepository.findOneById(id);
            if(book == null) {
                result.put("error", "Ничего не найдено!");
            }
            else {
                result.put("updateBook", book);
            }
        }
        else {
            List<Book> books = bookRepository.findBookByTitle(param);
            if(books.size() > 1) {
                result.put("error", "Найдено больше одного совпадения! Уточните запрос!");
            }
            else if(CollectionUtils.isEmpty(books)) {
                result.put("error", "Ничего не найдено!");
            }
            else {
                result.put("updateBook", books.get(0));
            }
        }

        return result;
    }

    public Map<String, String> updateBook(String title,
                                          String pages,
                                          String ISBN,
                                          String ISSN,
                                          String UDC,
                                          String issueYear,
                                          Date issueDate,
                                          String edition,
                                          String circulation,
                                          String kind,
                                          String series,
                                          String cover,
                                          String editionPlace,
                                          String publishingId,
                                          String id) {
        Map<String, String> status = new HashMap<>();

        int pagesInt, circulationInt, issueYearInt;
        Long publishingIdLong, idLong;
        if(StringUtils.isEmpty(title)) {
            status.put("error", "Заголовок не может быть пустым!");
            return status;
        }

        try {
            pagesInt = Integer.parseInt(pages);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение количества страниц!");
            return status;
        }

        try {
            circulationInt = Integer.parseInt(circulation);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение тиража!");
            return status;
        }

        try {
            issueYearInt = Integer.parseInt(issueYear);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение года издания!");
            return status;
        }

        try {
            publishingIdLong = Long.parseLong(publishingId);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение id издательства!");
            return status;
        }

        try {
            idLong = Long.parseLong(id);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение id!");
            return status;
        }

       int updatedBook = bookRepository.updateBook(title,
                pagesInt,
                ISBN,
                ISSN,
                UDC,
                issueYearInt,
                issueDate,
                edition,
                circulationInt,
                kind,
                series,
                cover,
                editionPlace,
                publishingIdLong,
                idLong);

        if(updatedBook > 0) {
            status.put("success", "Книга обновлена!");
        }

        return status;
    }

    public Map<String, String> addNewBook(String title, String pages, String issueYear) {
        Map<String, String> status = new HashMap<>();

        int pagesInt, issueYearInt;

        if(StringUtils.isEmpty(title)) {
            status.put("error", "Заголовок не может быть пустым!");
            return status;
        }

        try {
            pagesInt = Integer.parseInt(pages);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение количества страниц!");
            return status;
        }

        try {
            issueYearInt = Integer.parseInt(issueYear);
        }
        catch(Exception e) {
            status.put("error", "Некорректное значение года издания!");
            return status;
        }

        int newBook = newBookRepository.insertBook(title, pagesInt, issueYearInt);

        if(newBook > 0) {
            status.put("success", "Книга сохранена!");
        }
        else {
            status.put("error", "Не удалось сохранить книгу!");
        }

        return status;
    }

    public List<BookIssuedByReader> findByReader(Long id) {
        return bookIssuedByReaderRepository.findByReaderForm(id);
    }

    @Transactional
    public boolean issueBook(UserDto user, Long id) {
        String[] username = user.getUsername().split(" ");
        String[] password = user.getPassword().split(" ");
        ReaderForm readerForm = readerFormRepository.findBySurnameAndFirstNameAndPasportSerAndPasportNum(
                username[0],
                username[1],
                password[0],
                password[1]
        );

        int bookIssued = bookIssuedRepository.insertBookIssued(readerForm.getId(), id);

        if(bookIssued > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    @Transactional
    public boolean returnBook(UserDto user, Long id) {
        String[] username = user.getUsername().split(" ");
        String[] password = user.getPassword().split(" ");
        ReaderForm readerForm = readerFormRepository.findBySurnameAndFirstNameAndPasportSerAndPasportNum(
                username[0],
                username[1],
                password[0],
                password[1]
        );

        int bookIssued = bookIssuedRepository.deleteBookIssued(readerForm.getId(), id);

        if(bookIssued > 0) {
            return true;
        }
        else {
            return false;
        }
    }
}
