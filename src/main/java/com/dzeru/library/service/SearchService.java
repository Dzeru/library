package com.dzeru.library.service;

import com.dzeru.library.entity.FullBook;
import com.dzeru.library.entity.ShortBook;
import com.dzeru.library.repository.FullBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SearchService {

    @Autowired
    private FullBookRepository fullBookRepository;

    public Map<String, Object> search(String search) {
        List<FullBook> fullBooks = fullBookRepository.findByAllFields(search);
        List<ShortBook> shortBooks = fullBooks.stream().map(ShortBook::new).collect(Collectors.toList());
        Map<String, Object> searchAttributes = new HashMap<>();
        searchAttributes.put("list", shortBooks);
        searchAttributes.put("count", shortBooks.size());
        return searchAttributes;
    }

    public List<ShortBook> getAll() {
        List<FullBook> fullBooks = fullBookRepository.findAll();
        return fullBooks.stream().map(ShortBook::new).collect(Collectors.toList());
    }
}
