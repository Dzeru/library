package com.dzeru.library.service;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.Librarian;
import com.dzeru.library.entity.ReaderForm;
import com.dzeru.library.entity.Role;
import com.dzeru.library.repository.LibrarianRepository;
import com.dzeru.library.repository.ReaderFormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private LibrarianRepository librarianRepository;

    @Autowired
    private ReaderFormRepository readerFormRepository;

    public UserDto loadUser(String username, String password) throws UsernameNotFoundException {
        Librarian librarian = librarianRepository.findByLLoginAndLPassword(username, password);

        if(librarian != null) {
            return new UserDto(username, password, Role.ROLE_LIBRARIAN);
        }

        String[] usernameParts = username.split(" ");
        String[] passwordParts = password.split(" ");
        ReaderForm readerForm =
                readerFormRepository.findBySurnameAndFirstNameAndPasportSerAndPasportNum(
                        usernameParts[0],
                        usernameParts[1],
                        passwordParts[0],
                        passwordParts[1]
                );

        if(readerForm != null) {
            return new UserDto(username, password, Role.ROLE_READER);
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Librarian librarian = librarianRepository.findByLLogin(username);

        if(librarian != null) {
            return new UserDto(username, librarian.getLPassword(), Role.ROLE_LIBRARIAN);
        }

        String[] usernameParts = username.split(" ");
        ReaderForm readerForm =
                readerFormRepository.findBySurnameAndFirstName(
                        usernameParts[0],
                        usernameParts[1]
                );

        if(readerForm != null) {
            return new UserDto(username,
                    readerForm.getPasportSer() + " " + readerForm.getPasportNum(),
                    Role.ROLE_READER);
        }
        return null;
    }
}
