package com.dzeru.library.controller;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.Role;
import com.dzeru.library.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;

    @PostMapping("/search")
    public String search(@AuthenticationPrincipal UserDto user, @RequestParam String search, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, Object> searchAttributes = searchService.search(search);
        model.addAttribute("list", searchAttributes.get("list"));
        model.addAttribute("count", searchAttributes.get("count"));
        model.addAttribute("searchPart", search);

        return "search";
    }
}
