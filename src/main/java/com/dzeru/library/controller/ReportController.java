package com.dzeru.library.controller;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.Role;
import com.dzeru.library.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
@RequestMapping("/librarian")
public class ReportController {

    @Autowired
    private ReportService reportService;

    @GetMapping("/reports")
    public String reports(@AuthenticationPrincipal UserDto user,
                          Model model) throws Exception {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        model.addAttribute("countBooksByGenre", reportService.countBooksByGenre());
        Map<Integer, String> statusMap = reportService.checkBookTitles();
        if(statusMap.containsKey(0)) {
            model.addAttribute("successCheckBookTitles", statusMap.get(0));
        }
        else {
            model.addAttribute("errorCheckBookTitles", statusMap.get(1));
        }
        return "reports";
    }
}
