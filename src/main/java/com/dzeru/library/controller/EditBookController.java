package com.dzeru.library.controller;

import com.dzeru.library.dto.DeleteBookDto;
import com.dzeru.library.dto.UserDto;
import com.dzeru.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/librarian")
public class EditBookController {

    @Autowired
    private BookService bookService;

    @GetMapping("/editbook")
    public String editbook(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        return "editbook";
    }

    @PostMapping("/addbook")
    public String addBook(@AuthenticationPrincipal UserDto user,
                          @RequestParam String title,
                          @RequestParam String pages,
                          @RequestParam(required = false) String ISBN,
                          @RequestParam(required = false) String ISSN,
                          @RequestParam(required = false) String UDC,
                          @RequestParam(required = false) String issueYear,
                          @RequestParam(required = false, defaultValue = "2019-12-06") Date issueDate,
                          @RequestParam(required = false) String edition,
                          @RequestParam(required = false) String circulation,
                          @RequestParam(required = false) String kind,
                          @RequestParam(required = false) String series,
                          @RequestParam(required = false) String cover,
                          @RequestParam(required = false) String editionPlace,
                          @RequestParam(required = false) String publishingId,
                          Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, String> status = bookService.addBook(title, pages,
                ISBN, ISSN, UDC,
                issueYear, issueDate,
                edition, circulation, kind, series, cover,
                editionPlace, publishingId);

        if(status.containsKey("error")) {
            model.addAttribute("errorAddBook", status.get("error"));
        }

        if(status.containsKey("success")) {
            model.addAttribute("successAddBook", status.get("success"));
        }

        return "editbook";
    }

    @PostMapping("/getdeletebooks")
    public String getDeleteBooks(@AuthenticationPrincipal UserDto user,
                                 @RequestParam String title, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        List<DeleteBookDto> books = bookService.findBookByTitle(title);
        model.addAttribute("deleteBooks", books);

        return "editbook";
    }

    @PostMapping("/deletebooks")
    public String deleteBooks(@AuthenticationPrincipal UserDto user,
                              @RequestParam String bookIdsForDelete, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, String> status = bookService.deleteBooks(bookIdsForDelete);
        if(status.containsKey("error")) {
            model.addAttribute("errorDeleteBook", status.get("error"));
        }

        if(status.containsKey("success")) {
            model.addAttribute("successDeleteBook", status.get("success"));
        }
        return "editbook";
    }

    @PostMapping("/getupdatebooks")
    public String getUpdateBooks(@AuthenticationPrincipal UserDto user,
                                 @RequestParam String param, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, Object> result = bookService.findBookByTitleOrId(param);

        if(result.containsKey("error")) {
            model.addAttribute("errorUpdateBook", result.get("error"));
        }
        if(result.containsKey("updateBook")) {
            model.addAttribute("updateBook", result.get("updateBook"));
        }

        return "editbook";
    }

    @PostMapping("/updatebook")
    public String updateBook(@AuthenticationPrincipal UserDto user,
                             @RequestParam String title,
                             @RequestParam String pages,
                             @RequestParam(required = false) String ISBN,
                             @RequestParam(required = false) String ISSN,
                             @RequestParam(required = false) String UDC,
                             @RequestParam(required = false) String issueYear,
                             @RequestParam(required = false, defaultValue = "2019-12-06") Date issueDate,
                             @RequestParam(required = false) String edition,
                             @RequestParam(required = false) String circulation,
                             @RequestParam(required = false) String kind,
                             @RequestParam(required = false) String series,
                             @RequestParam(required = false) String cover,
                             @RequestParam(required = false) String editionPlace,
                             @RequestParam(required = false) String publishingId,
                             @RequestParam(required = false) String id,
                             Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, String> status = bookService.updateBook(title, pages,
                ISBN, ISSN, UDC,
                issueYear, issueDate,
                edition, circulation, kind, series, cover,
                editionPlace, publishingId, id);

        if(status.containsKey("error")) {
            model.addAttribute("errorUpdateBook", status.get("error"));
        }

        if(status.containsKey("success")) {
            model.addAttribute("successUpdateBook", status.get("success"));
        }

        return "editbook";
    }
}
