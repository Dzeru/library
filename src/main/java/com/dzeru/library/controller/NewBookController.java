package com.dzeru.library.controller;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.Role;
import com.dzeru.library.repository.NewBookRepository;
import com.dzeru.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.util.Map;

@Controller
public class NewBookController {

    @Autowired
    BookService bookService;

    @Autowired
    private NewBookRepository newBookRepository;


    @GetMapping("/newbooks")
    public String newBooks(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        model.addAttribute("newBooks", newBookRepository.findAll());

        return "newbooks";
    }

    @PostMapping("/librarian/addnewbook")
    public String addNewBook(@AuthenticationPrincipal UserDto user,
                          @RequestParam String title,
                          @RequestParam String pages,
                          @RequestParam(required = false) String ISBN,
                          @RequestParam(required = false) String ISSN,
                          @RequestParam(required = false) String UDC,
                          @RequestParam(required = false) String issueYear,
                          @RequestParam(required = false, defaultValue = "2019-12-06") Date issueDate,
                          @RequestParam(required = false) String edition,
                          @RequestParam(required = false) String circulation,
                          @RequestParam(required = false) String kind,
                          @RequestParam(required = false) String series,
                          @RequestParam(required = false) String cover,
                          @RequestParam(required = false) String editionPlace,
                          @RequestParam(required = false) String publishingId,
                          Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, String> status = bookService.addNewBook(title, pages, issueYear);

        if(status.containsKey("error")) {
            model.addAttribute("errorAddBook", status.get("error"));
        }

        if(status.containsKey("success")) {
            model.addAttribute("successAddBook", status.get("success"));
        }

        return "editnewbook";
    }

    @GetMapping("/librarian/editnewbook")
    public String editNewBook(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        return "editnewbook";
    }
}
