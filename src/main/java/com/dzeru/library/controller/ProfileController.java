package com.dzeru.library.controller;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.*;
import com.dzeru.library.repository.LibrarianRepository;
import com.dzeru.library.repository.LibraryCardRepository;
import com.dzeru.library.repository.LibraryInstanceRepository;
import com.dzeru.library.repository.ReaderFormRepository;
import com.dzeru.library.service.BookService;
import com.dzeru.library.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.Date;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class ProfileController {

    @Autowired
    private ReaderFormRepository readerFormRepository;

    @Autowired
    private LibrarianRepository librarianRepository;

    @Autowired
    private LibraryCardRepository libraryCardRepository;

    @Autowired
    private LibraryInstanceRepository libraryInstanceRepository;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private BookService bookService;

    @GetMapping("/librarian/profile/lib/{id}")
    public String profileLibrarian(@AuthenticationPrincipal UserDto user, @PathVariable("id") Long id, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Optional<Librarian> librarianOptional = librarianRepository.findById(id);
        librarianOptional.ifPresent(librarian -> model.addAttribute("profile", librarian));

        return "profilelibrarian";
    }

    @GetMapping("/librarian/profile/read/{id}")
    public String profileReader(@AuthenticationPrincipal UserDto user, @PathVariable("id") Long id, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Optional<ReaderForm> readerFormOptional = readerFormRepository.findById(id);

        if (readerFormOptional.isPresent()) {
            model.addAttribute("profile", readerFormOptional.get());
            LibraryCard libraryCard = libraryCardRepository.findByReaderFormId(readerFormOptional.get().getId());

            if (libraryCard != null) {
                model.addAttribute("libraryCard", libraryCard);
            }
        }

        return "profilereader";
    }

    @GetMapping("/profile")
    public String profile(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        if(user.getAuthorities().contains(Role.ROLE_READER)) {
            String[] username = user.getUsername().split(" ");
            String[] password = user.getPassword().split(" ");
            ReaderForm readerForm = readerFormRepository.findBySurnameAndFirstNameAndPasportSerAndPasportNum(
                    username[0],
                    username[1],
                    password[0],
                    password[1]
            );

            if (readerForm != null) {
                model.addAttribute("profile", readerForm);
                LibraryCard libraryCard = libraryCardRepository.findByReaderFormId(readerForm.getId());

                if(libraryCard != null) {
                    model.addAttribute("libraryCard", libraryCard);
                }

                List<BookIssuedByReader> booksIssued = bookService.findByReader(readerForm.getId());

                if(!CollectionUtils.isEmpty(booksIssued)) {
                    model.addAttribute("booksIssued", booksIssued);
                }

                return "profilereader";
            }
        }
        else if(user.getAuthorities().contains(Role.ROLE_LIBRARIAN)) {
            Librarian librarian = librarianRepository.findByLLoginAndLPassword(user.getUsername(), user.getPassword());

            if(librarian != null) {
                model.addAttribute("profile", librarian);
            }

            return "profilelibrarian";
        }

        return "redirect:/login";
    }

    @PostMapping("/updateprofilelibrarian")
    public String updateProfileLibrarian(@AuthenticationPrincipal UserDto user,
                                         String id,
                                         String surname,
                                         String firstName,
                                         String patronymic,
                                         String jobPosition,
                                         Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, Boolean> status = profileService.updateProfileLibrarian(
                id,
                surname, firstName, patronymic,
                jobPosition
        );

        if(status.containsKey("success")) {
            model.addAttribute("successUpdateProfile", "Профиль обновлен!");
        }

        if(status.containsKey("error")) {
            model.addAttribute("errorUpdateProfile", "Не удалось обновить профиль!");
        }

        return "profilelibrarian";
    }

    @PostMapping("/updateprofilereader")
    public String updateProfileReader(@AuthenticationPrincipal UserDto user,
                                      String id,
                                      String surname,
                                      String firstName,
                                      String patronymic,
                                      Date birthdayDate,
                                      String birthdayPlace,
                                      String education,
                                      String job,
                                      String phone,
                                      String email,
                                      Date registrationDate,
                                      String officialAddress,
                                      String factAddress,
                                      Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        Map<String, Boolean> status = profileService.updateProfileReader(
                id,
                surname, firstName, patronymic,
                birthdayDate, birthdayPlace,
                education, job,
                phone, email,
                registrationDate, officialAddress, factAddress
        );

        if(status.containsKey("success")) {
            model.addAttribute("successUpdateProfile", "Профиль обновлен!");
        }

        if(status.containsKey("error")) {
            model.addAttribute("errorUpdateProfile", "Не удалось обновить профиль!");
        }

        return "profilereader";
    }
}
