package com.dzeru.library.controller;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class BookIssuedController {

    @Autowired
    private BookService bookService;

    @GetMapping("/reader/issuebook/{id}")
    public String issueBook(@AuthenticationPrincipal UserDto user, @PathVariable Long id) {
       if(bookService.issueBook(user, id)) {
           return "redirect:/profile";
       }
       else {
           return "/";
       }
    }

    @GetMapping("/reader/returnbook/{id}")
    public String returnBook(@AuthenticationPrincipal UserDto user, @PathVariable Long id) {
        if(bookService.returnBook(user, id)) {
            return "redirect:/profile";
        }
        else {
            return "/";
        }
    }
}
