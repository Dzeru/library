package com.dzeru.library.controller;

import com.dzeru.library.dto.UserDto;
import com.dzeru.library.entity.Book;
import com.dzeru.library.entity.FullBook;
import com.dzeru.library.entity.Role;
import com.dzeru.library.repository.FullBookRepository;
import com.dzeru.library.repository.LibraryInstanceRepository;
import com.dzeru.library.repository.NewBookRepository;
import com.dzeru.library.repository.ShortBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {

    @Autowired
    private ShortBookRepository shortBookRepository;

    @Autowired
    private FullBookRepository fullBookRepository;

    @Autowired
    private LibraryInstanceRepository libraryInstanceRepository;

    @GetMapping("/")
    public String index(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        model.addAttribute("shortBooks", shortBookRepository.findAll());

        return "index";
    }

    @GetMapping("/fullbook/{id}")
    public String fullBook(@AuthenticationPrincipal UserDto user,
                           @PathVariable("id") Long id, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        model.addAttribute("fullbook", fullBookRepository.findOneById(id));
        model.addAttribute("libraryInstances", libraryInstanceRepository.findByBookId(id));

        return "fullbook";
    }

    @GetMapping("/loginlibrarian")
    public String loginLibrarian(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        model.addAttribute("librarianHeader", "Войти как библиотекарь");

        return "login";
    }

    @GetMapping("/loginreader")
    public String loginReader(@AuthenticationPrincipal UserDto user, Model model) {
        if(user != null) {
            model.addAttribute("profileNav", true);
        }

        model.addAttribute("readerHeader", "Войти как читатель");

        return "login";
    }
}
