package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Data
@Entity
public class LibraryCard {

    @Id
    private Long id;

    @Column(name = "issue_date")
    private Date issueDate;
}
