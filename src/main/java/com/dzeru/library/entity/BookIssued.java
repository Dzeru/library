package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Data
@Entity
public class BookIssued {
    @Id
    private Long id;

    @Column(name = "where_issued")
    private String whereIssued;

    @Column(name = "issue_date")
    private Date issueDate;

    @Column(name = "return_date")
    private Date returnDate;

    @Column(name = "library_instance_id")
    private Long libraryInstanceId;

    @Column(name = "librarian_id")
    private Long librarianId;

    @Column(name = "reader_form_id")
    private Long readerFormId;
}
