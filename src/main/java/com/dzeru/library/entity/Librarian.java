package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Librarian {

    @Id
    private Long id;

    private String surname;

    @Column(name = "first_name")
    private String firstName;

    private String patronymic;

    @Column(name = "job_position")
    private String jobPosition;

    @Column(name = "l_login")
    private String lLogin;

    @Column(name = "l_password")
    private String lPassword;
}
