package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Data
@Entity
public class Book {
    @Id
    private Long id;

    private String title;
    private String ISBN;
    private String ISSN;
    private String UDC;

    @Column(name = "issue_year")
    private int issueYear;

    @Column(name = "issue_date")
    private Date issueDate;

    private String edition;
    private int pages;
    private int circulation;
    private String series;
    private String kind;
    private String cover;

    @Column(name = "edition_place")
    private String editionPlace;

    @Column(name = "publishing_id")
    private Long publishingId;
}
