package com.dzeru.library.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ROLE_LIBRARIAN, ROLE_READER;

    @Override
    public String getAuthority() {
        return name();
    }
}