package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Data
@Entity
public class ReaderForm {
    @Id
    private Long id;

    private String surname;

    @Column(name = "first_name")
    private String firstName;

    private String patronymic;

    @Column(name = "birthday_date")
    private Date birthdayDate;

    @Column(name = "birthday_place")
    private String birthdayPlace;

    private String education;

    private String job;

    private String phone;

    private String email;

    @Column(name = "pasport_ser")
    private String pasportSer;

    @Column(name = "pasport_num")
    private String pasportNum;

    @Column(name = "pasport_when")
    private Date pasportWhen;

    @Column(name = "pasport_who")
    private String pasportWho;

    @Column(name = "registration_date")
    private Date registrationDate;

    @Column(name = "official_address")
    private String officialAddress;

    @Column(name = "fact_address")
    private String factAddress;
}
