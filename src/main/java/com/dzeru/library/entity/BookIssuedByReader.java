package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;

@Data
@Entity
public class BookIssuedByReader {
    @Id
    private long id;

    @Column(name = "where_issued")
    private String whereIssued;

    @Column(name = "issue_date")
    private Date issueDate;

    @Column(name = "return_date")
    private Date returnDate;

    @Column(name = "li_id")
    private Long liId;

    @Column(name = "b_id")
    private Long bId;

    private String LK;
    private String copyright;

    @Column(name = "get_out")
    private boolean getOut;

    private String title;

    @Column(name = "issue_year")
    private int issueYear;

    private int pages;
    private String series;

    private String surname;
    private String patronymic;

    @Column(name = "first_name")
    private String firstName;
}
