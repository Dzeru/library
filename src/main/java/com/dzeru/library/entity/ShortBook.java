package com.dzeru.library.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class ShortBook {
    @Id
    private Long id;

    private String title;
    private int pages;

    @Column(name = "issue_year")
    private int issueYear;

    private String surname;

    @Column(name = "first_name")
    private String firstName;

    public ShortBook(FullBook fullBook) {
        this.id = fullBook.getId();
        this.title = fullBook.getTitle();
        this.pages = fullBook.getPages();
        this.issueYear = fullBook.getIssueYear();
        this.surname = fullBook.getSurname();
        this.firstName = fullBook.getFirstName();
    }

}
