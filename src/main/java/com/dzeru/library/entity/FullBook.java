package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Date;
import java.time.OffsetDateTime;

@Data
@Entity
public class FullBook {
    @Id
    private Long id;

    private String title;
    private String ISBN;
    private String ISSN;
    private String UDC;

    @Column(name = "issue_year")
    private int issueYear;

    @Column(name = "issue_date")
    private Date issueDate;

    private String edition;
    private int pages;
    private int circulation;
    private String series;
    private String kind;
    private String cover;

    @Column(name = "edition_place")
    private String editionPlace;

    @Column(name = "publishing_id")
    private Long publishingId;

    private String surname;
    private String patronymic;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "author_id")
    private Long authorId;

    private String country;

    @Column(name = "lang_id")
    private Long langId;

    private String lang;

    @Column(name = "genre_id")
    private Long genreId;

    private String genre;

    @Column(name = "publish_id")
    private Long publishId;

    @Column(name = "publishing_title")
    private String publishingTitle;

    @Column(name = "publishing_country")
    private String publishingCountry;

    private String city;
    private String street;

    @Column(name = "house_number")
    private String houseNumber;
}
