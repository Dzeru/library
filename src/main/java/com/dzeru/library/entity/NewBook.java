package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class NewBook {
    @Id
    private Long id;

    private String title;

    @Column(name = "issue_year")
    private int issueYear;

    private int pages;
}
