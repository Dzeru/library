package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class LibraryInstance {
    @Id
    private Long id;

    private String LK;
    private String copyright;

    @Column(name = "get_out")
    private boolean getOut;
}
