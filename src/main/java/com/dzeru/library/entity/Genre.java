package com.dzeru.library.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Genre {
    @Id
    private Long id;

    private String genre;
}
