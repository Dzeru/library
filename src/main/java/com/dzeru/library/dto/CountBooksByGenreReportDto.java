package com.dzeru.library.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountBooksByGenreReportDto {
    String genre;
    int count;

    public CountBooksByGenreReportDto(String genre, int count) {
        this.genre = genre;
        this.count = count;
    }
}
