package com.dzeru.library.dto;

import com.dzeru.library.entity.Book;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DeleteBookDto {
    private Long id;
    private String title;

    public DeleteBookDto(Book book) {
        this.id = book.getId();
        this.title = book.getTitle();
    }
}
