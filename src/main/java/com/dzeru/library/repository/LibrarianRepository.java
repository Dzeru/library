package com.dzeru.library.repository;

import com.dzeru.library.entity.Librarian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;

@Repository
public interface LibrarianRepository extends JpaRepository<Librarian, Long> {

    @Query(value = "select * from librarian where l_login = :lLogin and l_password = :lPassword", nativeQuery = true)
    Librarian findByLLoginAndLPassword(@Param("lLogin") String login,
                                       @Param("lPassword") String password);

    @Query(value = "select * from librarian where l_login = :lLogin", nativeQuery = true)
    Librarian findByLLogin(String username);

    @Query(value = "update librarian set surname = :surname, first_name = :firstName, patronymic = :patronymic, " +
            "job_position = :jobPosition where id = :id ;\n" +
            "select @@ROWCOUNT;", nativeQuery = true)
    int updateLibrarian(@Param("id") String id,
                              @Param("surname") String surname,
                              @Param("firstName") String firstName,
                              @Param("patronymic") String patronymic,
                              @Param("jobPosition") String jobPosition);
}
