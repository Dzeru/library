package com.dzeru.library.repository;

import com.dzeru.library.entity.ShortBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShortBookRepository extends JpaRepository<ShortBook, Long> {
    @Query(value = "select * from short_book", nativeQuery = true)
    List<ShortBook> findAll();
}
