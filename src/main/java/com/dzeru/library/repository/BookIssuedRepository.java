package com.dzeru.library.repository;

import com.dzeru.library.entity.BookIssued;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BookIssuedRepository extends JpaRepository<BookIssued, Long> {

    @Query(value = "begin try insert into book_issued (where_issued, issue_date, return_date, library_instance_id, reader_form_id)\n" +
            "values ('прил', getdate(), dateadd(day, 30, getdate()), :libraryInstanceId, :readerFormId );\n" +
            "select 1; end try begin catch select 0; end catch;", nativeQuery = true)
    int insertBookIssued(@Param("readerFormId") Long readerFormId,
                         @Param("libraryInstanceId") Long libraryInstanceId);

    @Query(value = "declare @did int;\n" +
            "select @did = d.id from debt d join book_issued b on d.book_issued_id = b.id where b.reader_form_id = :readerFormId and d.book_issued_id = :id ;\n" +
            "delete from debt_notification where debt_id = @did;\n" +
            "delete from debt where id = @did;\n" +
            "delete from book_issued where id = :id ;" +
            "select @@ROWCOUNT;", nativeQuery = true)
    int deleteBookIssued(@Param("readerFormId") Long readerFormId,
                         @Param("id") Long id);
}
