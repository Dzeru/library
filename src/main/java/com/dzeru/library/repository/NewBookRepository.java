package com.dzeru.library.repository;

import com.dzeru.library.entity.NewBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NewBookRepository extends JpaRepository<NewBook, Long> {

    @Query(value = "begin try insert into new_books (book.title, book.issue_year, book.pages) " +
            "values ( :title, :issueYear, :pages ); select 1; end try begin catch select 0; end catch;", nativeQuery = true)
    int insertBook(@Param("title") String title,
                   @Param("pages") int pages,
                   @Param("issueYear") int issueYear);

    @Query(value = "select * from new_books;", nativeQuery = true)
    List<NewBook> findAll();
}
