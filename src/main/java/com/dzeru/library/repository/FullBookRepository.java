package com.dzeru.library.repository;

import com.dzeru.library.entity.FullBook;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FullBookRepository extends JpaRepository<FullBook, Long> {
    @Query(value = "select id, title, ISBN, ISSN, UDC, issue_year, issue_date, edition, pages, circulation, series, kind, cover,\n" +
            "edition_place, publishing_id, author_id, surname, first_name, patronymic, country, lang,\n" +
            "publishing_title, publishing_country, city, street, house_number, publish_id, lang_id,\n" +
            "min(genre_id) as genre_id, genre = \n" +
            "replace((select genre as [data()] from full_book where  id = f.id order by id for xml path('')), ' ', ', ')\n" +
            "from full_book f where id = :id\n" +
            "group by id, title, ISBN, ISSN, UDC, issue_year, issue_date, edition, pages, circulation, series, kind, cover,\n" +
            "edition_place, publishing_id, author_id, surname, first_name, patronymic, country, lang,\n" +
            "publishing_title, publishing_country, city, street, house_number, publish_id, lang_id;", nativeQuery = true)
    FullBook findOneById(@Param("id") Long id);

    @Query(value = "select * from full_book", nativeQuery = true)
    List<FullBook> findAll();
    
    @Query(value = "select id, title, ISBN, ISSN, UDC, issue_year, issue_date, edition, pages, circulation, series, kind, cover,\n" +
            "edition_place, publishing_id, author_id, surname, first_name, patronymic, country, lang,\n" +
            "publishing_title, publishing_country, city, street, house_number, publish_id, lang_id,\n" +
            "min(genre_id) as genre_id, genre = \n" +
            "replace((select genre as [data()] from full_book where  id = f.id order by id for xml path('')), ' ', ', ')\n" +
            "from full_book f\n" +
            "where title like concat('%', :search, '%') or\n" +
            "series like concat('%', :search, '%') or\n" +
            "kind like concat('%', :search, '%') or\n" +
            "edition_place like concat('%', :search, '%') or\n" +
            "lang like concat('%', :search, '%') or\n" +
            "surname like concat('%', :search, '%') or\n" +
            "first_name like concat('%', :search, '%') or\n" +
            "patronymic like concat('%', :search, '%') or\n" +
            "publishing_title like concat('%', :search, '%') or\n" +
            "country like concat('%', :search, '%') or\n" +
            "city like concat('%', :search, '%')\n" +
            "group by id, title, ISBN, ISSN, UDC, issue_year, issue_date, edition, pages, circulation, series, kind, cover,\n" +
            "edition_place, publishing_id, author_id, surname, first_name, patronymic, country, lang,\n" +
            "publishing_title, publishing_country, city, street, house_number, publish_id, lang_id",
            nativeQuery = true)
    List<FullBook> findByAllFields(@Param("search") String search);
}

