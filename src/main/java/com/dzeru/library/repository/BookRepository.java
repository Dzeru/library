package com.dzeru.library.repository;

import com.dzeru.library.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    @Query(value = "declare @title_status int;\n" +
            "exec @title_status = CheckBookTitles;\n" +
            "select 'Title Status' = @title_status;", nativeQuery = true)
    int checkBookTitles();

    @Query(value = "begin try\n" +
            "insert into book (title, ISBN, ISSN, UDC,\n" +
            "issue_year, issue_date,\n" +
            "edition, pages, circulation,\n" +
            "series, kind, cover,\n" +
            "edition_place, publishing_id)\n" +
            "values ( :title, :ISBN, :ISSN, :UDC,\n" +
            ":issueYear, :issueDate,\n" +
            ":edition, :pages, :circulation, :series, :kind, :cover,\n" +
            ":editionPlace, :publishingId) ; select 1; end try begin catch select 0; end catch;", nativeQuery = true)
    int insertBook(@Param("title") String title,
                    @Param("pages") int pages,
                    @Param("ISBN") String ISBN,
                    @Param("ISSN") String ISSN,
                    @Param("UDC") String UDC,
                    @Param("issueYear") int issueYear,
                    @Param("issueDate") Date issueDate,
                    @Param("edition") String edition,
                    @Param("circulation") int circulation,
                    @Param("kind") String kind,
                    @Param("series") String series,
                    @Param("cover") String cover,
                    @Param("editionPlace") String editionPlace,
                    @Param("publishingId") Long publishingId);

    @Query(value = "select * from book where title like concat('%', :title, '%');", nativeQuery = true)
    List<Book> findBookByTitle(@Param("title") String title);

    @Query(value = "delete from book_author where book_id in (:ids);\n" +
            "delete from book_lang where book_id in (:ids);\n" +
            "delete from book_genre where book_id in (:ids);\n" +
            "delete from library_instance where book_id in (:ids);\n" +
            "delete from book_order_position where book_id in (:ids);\n" +
            "delete from book where id in (:ids);\n" +
            "select @@ROWCOUNT;", nativeQuery = true)
    int deleteBooksById(@Param("ids") List<Long> idsLong);

    @Query(value = "select * from book where id = :id ;", nativeQuery = true)
    Book findOneById(@Param("id") Long id);

    @Query(value = "update book set title = :title, ISBN = :ISBN, ISSN = :ISSN, UDC = :UDC, \n" +
            "issue_year = :issueYear, issue_date = :issueDate,\n" +
            "edition = :edition, pages = :pages, circulation = :circulation,\n" +
            "series = :series, kind = :kind, cover = :cover,\n" +
            "edition_place = :editionPlace, publishing_id = :publishingId where id = :id ;\n" +
            "select @@ROWCOUNT;", nativeQuery = true)
    int updateBook(@Param("title") String title,
                    @Param("pages") int pages,
                    @Param("ISBN") String ISBN,
                    @Param("ISSN") String ISSN,
                    @Param("UDC") String UDC,
                    @Param("issueYear") int issueYear,
                    @Param("issueDate") Date issueDate,
                    @Param("edition") String edition,
                    @Param("circulation") int circulation,
                    @Param("kind") String kind,
                    @Param("series") String series,
                    @Param("cover") String cover,
                    @Param("editionPlace") String editionPlace,
                    @Param("publishingId") Long publishingId,
                    @Param("id") Long id);
}
