package com.dzeru.library.repository;

import com.dzeru.library.entity.LibraryInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LibraryInstanceRepository extends JpaRepository<LibraryInstance, Long> {

    @Query(value = "select * from library_instance li left join " +
            "book_issued bi on bi.library_instance_id = li.id " +
            "where bi.id is null and book_id = :bookId", nativeQuery = true)
    List<LibraryInstance> findByBookId(@Param("bookId") Long bookId);
}
