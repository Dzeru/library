package com.dzeru.library.repository;

import com.dzeru.library.entity.ReaderForm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Date;

@Repository
public interface ReaderFormRepository extends JpaRepository<ReaderForm, Long> {
    @Query(value = "select * from reader_form where\n" +
            "surname = :surnm and first_name = :firstnm\n" +
            "and pasport_ser = :pasportSer and pasport_num = :pasportNum"
            , nativeQuery = true)
    ReaderForm findBySurnameAndFirstNameAndPasportSerAndPasportNum(@Param("surnm") String surname,
                                                                   @Param("firstnm") String firstName,
                                                                   @Param("pasportSer") String pasportSer,
                                                                   @Param("pasportNum") String pasportNum);

    @Query(value = "select * from reader_form where\n" +
            "surname = :surnm and first_name = :firstnm"
            , nativeQuery = true)
    ReaderForm findBySurnameAndFirstName(@Param("surnm") String surname,
                                         @Param("firstnm") String firstName);

    @Query(value = "update reader_form set surname = :surname, first_name = :firstName, patronymic = :patronymic, \n" +
            "birthday_place = :birthdayPlace, birthday_date = :birthdayDate,\n" +
            "education = :education, job = :job, phone = :phone, email = :email,\n" +
            "registration_date = :registrationDate, official_address = :officialAddress, fact_address = :factAddress\n" +
            " where id = :id ;\n" +
            "select @@ROWCOUNT;", nativeQuery = true)
    int updateReader(@Param("id") String id,
                     @Param("surname") String surname,
                     @Param("firstName") String firstName,
                     @Param("patronymic") String patronymic,
                     @Param("birthdayDate") Date birthdayDate,
                     @Param("birthdayPlace") String birthdayPlace,
                     @Param("education") String education,
                     @Param("job") String job,
                     @Param("phone") String phone,
                     @Param("email") String email,
                     @Param("registrationDate") Date registrationDate,
                     @Param("officialAddress") String officialAddress,
                     @Param("factAddress") String factAddress);
}
