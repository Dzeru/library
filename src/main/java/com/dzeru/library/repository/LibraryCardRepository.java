package com.dzeru.library.repository;

import com.dzeru.library.entity.LibraryCard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryCardRepository extends JpaRepository<LibraryCard, Long> {

    @Query(value = "select lc.id, lc.issue_date from library_card lc inner join reader_form rf on rf.library_card_id = lc.id where rf.id = :id", nativeQuery = true)
    LibraryCard findByReaderFormId(@Param("id") Long id);
}
