package com.dzeru.library.repository;

import com.dzeru.library.entity.BookIssuedByReader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookIssuedByReaderRepository extends JpaRepository<BookIssuedByReader, Long> {

    @Query(value = "select \n" +
            "bi.id, bi.where_issued, bi.issue_date, bi.return_date,\n" +
            "li.id as li_id, li.lk, li.copyright, li.get_out,\n" +
            "b.id as b_id, b.title, b.series, b.pages, b.issue_year,\n" +
            "a.surname, a.first_name, a.patronymic\n" +
            "from book_issued bi\n" +
            "left join library_instance li on li.id = bi.library_instance_id\n" +
            "left join book b on b.id = li.book_id\n" +
            "left join book_author ba on ba.book_id = b.id\n" +
            "left join author a on ba.author_id = a.id\n" +
            "where bi.reader_form_id = :readerFormId", nativeQuery = true)
    List<BookIssuedByReader> findByReaderForm(@Param("readerFormId") Long readerFormId);
}
